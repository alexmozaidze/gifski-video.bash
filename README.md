# gifski-video

A utility script that allows you to convert **any** video file into GIF.

## Requirements

- bash
- ffmpeg
- gifski

## Usage

```sh
gifski-video <FFMPEG-OPTS> --- <GIFSKI-OPTS>
```

## Sample Usage

```sh
gifski-video -i input.mp4 --- -o output.gif
```

## Notes

### cancelling/aborting

If you hit `Ctrl-C` while the gif is generating, it will leave a hanging gif that will, most likely, not be animated.

### custom delimiter

If you want to change the default delimiter, change the `delimiter` constant that's defined at the very top of the script.
